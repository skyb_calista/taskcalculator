import data.ChekInputStream;

import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import result.GetResult;

import java.util.regex.Pattern;

public class Calculator {
    static Scanner scanner = new Scanner(System.in);
    private static final Pattern ROMAN_DIGIT = Pattern.compile("^(?i)M{0,3}(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(V?I{0,3}|I[VX])$");
    private static final Logger LOGGER = LoggerFactory.getLogger(Calculator.class);

    public static void main(String[] args) {
        LOGGER.info("Введите математику:");
        String string = scanner.next();
        String[] split = string.split("-|\\+|/|\\*");
        if(ChekInputStream.romanSymbol(split, ROMAN_DIGIT) && ChekInputStream.arabikSymbol(split)) {
            throw new ArithmeticException("Вводимые значения должны быть или арабские или римскими");
        }
        else if(ChekInputStream.romanSymbol(split, ROMAN_DIGIT)) {
            int result = GetResult.getRomanResult(string, split);
            LOGGER.info("Результат: " + result);
        }
        else if(ChekInputStream.arabikSymbol(split)) {
            int result = GetResult.getArabicResult(string, split);
            LOGGER.info("Результат: " + result);
        }
        else {
            throw new ArithmeticException("Введены не верные значения");
        }
    }
}
