package result;

import model.RomanToArabic;

public class GetResult {

    public static int getArabicResult(String string, String[] split) {
        String[] splitMinus = string.split("-");
        String[] splitPlus = string.split("\\+");
        String[] splitMultiply = string.split("\\*");
        String[] splitDivide = string.split("/");
        int num1 = Integer.parseInt(split[0]);
        int num2 = Integer.parseInt(split[1]);
        if(num1 < 1 || num1 > 10 || num2 < 1 || num2 > 10) {
            throw new IllegalArgumentException("Вводимое число должно быть от 1 до 10");
        }
        int value ;
        if(splitMinus.length > 1){
            value = num1 - num2;
        }
        else if(splitPlus.length > 1){
            value = num1 + num2;
        }
        else if(splitMultiply.length > 1){
            value = num1 + num2;
        }
        else if(splitDivide.length > 1){
            value = num1 + num2;
        }
        else {
            throw new IllegalArgumentException("Не допустимая операция");
        }
        return value;
    }

    public static int getRomanResult(String string, String[] split) {
        String[] splitMinus = string.split("-");
        String[] splitPlus = string.split("\\+");
        String[] splitMultiply = string.split("\\*");
        String[] splitDivide = string.split("/");
        int value;
        int num1 = RomanToArabic.convert(split[0]);
        int num2 = RomanToArabic.convert(split[1]);
        if(num1 < 1 || num1 > 10 || num2 < 1 || num2 > 10) {
            throw new IllegalArgumentException("Вводимое число должно быть от 1 до 10");
        }
        if(splitMinus.length > 1){
            value = num1 - num2;
        }
        else if(splitPlus.length > 1){
            value = num1 + num2;
        }
        else if(splitMultiply.length > 1){
            value = num1 * num2;
        }
        else if(splitDivide.length > 1){
            value = num1 / num2;
        }
        else {
            throw new IllegalArgumentException("Не допустимая операция");
        }
        return value;
    }
}
