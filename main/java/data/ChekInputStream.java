package data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChekInputStream {
    public static boolean arabikSymbol(String[] split) {
        boolean check = false;
        for(String s : split) {
            if(isDigit(s)) {
                check = true;
            }
        }
        return check;
    }
    public static boolean romanSymbol(String[] split, Pattern ROMAN_DIGIT) {
        boolean check = false;
        for(String s : split) {
            Matcher matcher = ROMAN_DIGIT.matcher(s);
            if(matcher.find()) {
                check = true;
            }
        }
        return check;
    }

    public static boolean isDigit(String s) throws NumberFormatException {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
    }
}
